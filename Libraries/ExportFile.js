
function generateText(tasks){
    var lines="";

    // Creating iterator
    var iteratorPattern = new IteratorPattern(tasks);



    for (var item = iteratorPattern.first(); iteratorPattern.hasNext(); item = iteratorPattern.next()) {

        lines+=item.name+" "+item.priority+" "+item.taskTime+" "+item.status+"\r\n";

    }
    return lines;
}




function saveTextAsFile(textToWrite)
{
    var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
    var fileNameToSaveAs = "savedtasks.txt";
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";
    if (window.webkitURL != null)
    {
        // Chrome allows the link to be clicked
        // without actually adding it to the DOM.
        downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
    }
    else
    {
        // Firefox requires the link to be added to the DOM
        // before it can be clicked.
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        var event = new MouseEvent('click');
        downloadLink.dispatchEvent(event);
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
    }

    document.body.removeChild(downloadLink);
}
