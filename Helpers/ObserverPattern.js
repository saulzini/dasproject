
var Subject = function() {
  observers = [];

  return {
    subscribeObserver: function(observer) {
      observers.push(observer);
    },
    unsubscribeObserver: function(observer) {
      var index = observers.indexOf(observer);
      if(index > -1) {
        observers.splice(index, 1);
      }
    },
    notifyObserverTable: function(observer,updateByTime) {
      var index = observers.indexOf(observer);
      if(index > -1) {
        observers[index].notify(index,updateByTime);
      }
    },
    notifyObserverSelect: function(observer) {
      var index = observers.indexOf(observer);
      if(index > -1) {
        observers[index].notify(index);
      }
    },
    notifyAllObserversSelects: function() {
      for(var i = 0; i < observers.length; i++){
        observers[i].notify(i);
      };
    }
  };
};


var ObserverTable = function() {
  return {
    notify: function(index,updateByTime) {
      console.log("Observer table" + index + " is notified!");

      var table = document.getElementById("tableBody");

      // Creating iterator
      var iteratorPattern = new IteratorPattern(tasks);


      var elements = [];
      var index = 0;
      for (var item = iteratorPattern.first(); iteratorPattern.hasNext(); item = iteratorPattern.next(), index++) {

          if (updateByTime == true ) {

              if (item.countingDown == true) {

                  item.decrementCountDown();
              }
              if (item.countingUp == true) {
                  item.incrementCountUp();
              }
          }


          var itemAttributes  ="<tr><td>"+
              item.name+
              "</td><td>"+
              item.priority+
              "</td><td>" +
              item.taskTime+
              "</td><td>"+
              item.countUp+
              "</td><td>" +
              item.countDown+
              "</td><td>"+
              item.status+
              "</td><td>";

          var buttonCountingUp = obtainButtonCountUp(item, index);
          var buttonCountingDown = obtainButtonCountDown(item, index);
          var buttonReset = obtainButtonReset(item, index);


          elements.push( itemAttributes+buttonCountingUp+buttonCountingDown+buttonReset+ "</td> </tr>"  );



      }

      table.innerHTML = elements;


    }
  }
}



var ObserverSelect = function(idSelect) {
  return {
    notify: function(index) {
      console.log("Observer select" + index + " is notified!");

      var select = document.getElementById(idSelect);
      // Creating iterator
      var iteratorPattern = new IteratorPattern(tasks);


      var elements = "";
      var index = 0;
      for (var item = iteratorPattern.first(); iteratorPattern.hasNext(); item = iteratorPattern.next(), index++) {

          elements+= '<option value="'+ index +'">'+item.name+'</option>';

      }

      select.innerHTML = elements;
       

      }
  }
}




function obtainButtonCountUp(item, index) {
    var buttonCountingUp= "";

    if (item.countingUp == true) {
        buttonCountingUp = "<button type='button' class='btn btn-danger' onclick=stopCountingUp(" + index + ",event)>Stop count up</button> ";

    }
    else {
        buttonCountingUp = "<button type='button' class='btn btn-primary' onclick=startCountingUp(" + index + ",event)>Start count up</button> ";

    }
    return buttonCountingUp;
}
function obtainButtonCountDown(item, index) {
    var buttonCountingDown = "";
    if (item.countingDown == true) {
        buttonCountingDown = "<button type='button' class='btn btn-danger' onclick=stopCountingDown(" + index + ",event)>Stop count down</button>";
    }
    else {
        buttonCountingDown = "<button type='button' class='btn btn-primary' onclick=startCountingDown(" + index + ",event)>Start count down</button> ";


    }
    return buttonCountingDown;
}
function obtainButtonReset(item, index) {
    var buttonReset = "";
    if (item.countingDown == false && item.countingUp==false) {
        buttonReset = "<button type='button' class='btn btn-default' onclick=resetTime(" + index + ",event)>Reset time</button>";
    }

    return buttonReset;
}
