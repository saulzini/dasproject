

var IteratorPattern = function(elements) {
    this.index = 0;
    this.elements = elements;
};

IteratorPattern.prototype = {
    first: function() {
        this.reset();
        return this.next();
    },
    next: function() {
        return this.elements[this.index++];
    },
    hasNext: function() {
        return this.index <= this.elements.length;
    },
    isLast:function(){
        return this.index-1==this.elements.length-1;
    },
    reset: function() {
        this.index = 0;
    }
};
