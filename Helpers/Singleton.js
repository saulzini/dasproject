var Singleton = (function() {
 
    //Singleton constructor is private
    function Singleton() {
        this.infiniteTimer = new InfiniteTimer();
        this.idTimer = 0;
    }
 
    //private var to store the single instance
    var singleInstance;
 
    //Return the object providing getInstance method
    return {
        getInstance: function() {
            if (!singleInstance) singleInstance = new Singleton();
            return singleInstance;
 
        }
    }
 
})()