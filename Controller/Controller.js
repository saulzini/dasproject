


var tasks=[ new Task("hello","High",12) , new Task("Task","Medium",12) ];

//singleton pattern
var instanceSingletonTimer = Singleton.getInstance();

//observer pattern
var subject = new Subject();
var observerTable1 = new ObserverTable();
var observerSelectUpdate = new ObserverSelect("nameSelectEdit");
var observerSelectDelete = new ObserverSelect("nameSelectDelete");

subject.subscribeObserver(observerTable1);
subject.subscribeObserver(observerSelectUpdate);
subject.subscribeObserver(observerSelectDelete);




function orderTasksByPriorities(){
     // Creating iterator
      var iteratorPattern = new IteratorPattern(tasks);
        var high=[];
        var medium=[];
        var low=[];
        for (var task = iteratorPattern.first(); iteratorPattern.hasNext(); task = iteratorPattern.next()) {
            if(task.priority=="High")
                high.push(task);
            if(task.priority=="Medium")
                medium.push(task);
            if(task.priority=="Low")
                low.push(task);
        }
        newTasks=[];

        Array.prototype.push.apply(newTasks, high);
        Array.prototype.push.apply(newTasks, medium);
        Array.prototype.push.apply(newTasks, low);
    
        
        tasks = newTasks;
    
}

function updateElementsInPage() {
   
   //update table
    subject.notifyObserverTable(observerTable1,false);
   //update selects
    subject.notifyObserverSelect(observerSelectUpdate);
    subject.notifyObserverSelect(observerSelectDelete); 
   
}
function addNewTask(event) {
    event.preventDefault();


    var nameCreate = document.getElementById("nameCreate").value;
    var priorityCreate = document.getElementById("priorityCreate").value;
    var taskTimeCreate = document.getElementById("taskTimeCreate").value;

    tasks.push(new Task( nameCreate,priorityCreate,taskTimeCreate  ));
    orderTasksByPriorities();
    updateElementsInPage();

}
function editTask(event) {
    event.preventDefault();


    var nameSelectValue= document.getElementById("nameSelectEdit").value;
    var nameEdit = document.getElementById("nameEdit").value;
    var priorityEdit= document.getElementById("priorityEdit").value;
    var taskTimeEdit= document.getElementById("taskTimeEdit").value;
    var statusEdit= document.getElementById("statusEdit").value;

    tasks[nameSelectValue].edit(nameEdit,priorityEdit,taskTimeEdit,statusEdit);
    orderTasksByPriorities();

    updateElementsInPage();



}
function deleteTask(event) {
    event.preventDefault();


    var nameSelectValue= document.getElementById("nameSelectDelete").value;

    tasks.splice(nameSelectValue, 1);

    updateElementsInPage();
}



function startCountingUp(index,event) {
    event.preventDefault();

    if(tasks[index].status != "Completed") {

        tasks[index].incrementCountUp();
        //update table
        subject.notifyObserverTable(observerTable1,false);
    }
    else {
        window.alert("The task is already completed");

    }
}
function  stopCountingUp(index,event) {
    event.preventDefault();

    tasks[index].stopCountingUp();
   //update table
    subject.notifyObserverTable(observerTable1,false);
}
function startCountingDown(index,event) {
    event.preventDefault();

    if(tasks[index].status != "Completed") {
        tasks[index].decrementCountDown();
       //update table
        subject.notifyObserverTable(observerTable1,false);
    }

    else {
        window.alert("The task is already completed");

    }
}
function  stopCountingDown(index,event) {
    event.preventDefault();

    tasks[index].stopCountingDown();
   //update table
    subject.notifyObserverTable(observerTable1,false);
}
function  resetTime(index,event) {
    event.preventDefault();


    tasks[index].resetAttributes();
   //update table
    subject.notifyObserverTable(observerTable1,false);
}




////////////////////////////////////Timer//////////////////////////////////////////

function startTimer(event){
    event.preventDefault();
    
        instanceSingletonTimer.infiniteTimer.idTimer= setInterval("instanceSingletonTimer.infiniteTimer.incrementCountUp()",1000);
    
}
function stopTimer(event){
    event.preventDefault();
    clearInterval(instanceSingletonTimer.infiniteTimer.idTimer);
    instanceSingletonTimer.infiniteTimer.idTimer=0;
}
function resetTimer(event){
    event.preventDefault();

    instanceSingletonTimer.infiniteTimer.resetCountUp();

    clearInterval(instanceSingletonTimer.infiniteTimer.idTimer);
}
function updateTimer(){
    var timer = document.getElementById("timer");

    timer.innerHTML = "Time: "+instanceSingletonTimer.infiniteTimer.currentTime+" seg";
}

//////////////////////////////////////Save to file////////////////////////////////////////
function saveToFile(event){
    event.preventDefault();

    saveTextAsFile(generateText(tasks));
}

////////////////////////////////////////////////////////////////////////////////////
window.onload = function() {

    updateElementsInPage();
    setInterval("subject.notifyObserverTable(observerTable1,true)",1000);
    setInterval("updateTimer(true)",1000);


};