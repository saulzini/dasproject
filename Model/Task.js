
class Task{


    constructor( name,priority,taskTime) {


        if(name==""){
            name = "No name";
        }
        if(taskTime==""){
            taskTime = 10;
        }

        this.name = name;
        this.priority = priority;
        this.taskTime = taskTime;

        this.countUp = 0;
        this.countDown= taskTime;
        this.status = "Incomplete";

        this.countingUp = false;
        this.countingDown= false;

    }

    setStatus(status){
        this.status = status;

    }

    setPriority(priority){
        this.priority = priority;

    }

    resetCountUp(){
        this.countUp=0;
        this.countingUp = false;

    }

    resetCountDown(){
        this.countDown=this.taskTime;
        this.countingDown= false;
    }

    resetAttributes(){
        this.resetCountUp();
        this.resetCountDown();

        this.setStatus("Incomplete");

    }


    incrementCountUp(){

        if (this.countUp >= this.taskTime ){
            window.alert("Task count up finished:"+this.name);
            this.countingUp=false;
            this.setStatus("Completed");

        }else {

            this.countUp++;
            this.countingUp= true;
        }


    }

    decrementCountDown(){


        if (this.countDown <= 0){
            window.alert("Task count down finished:"+this.name);
            this.countingDown = false;
            this.setStatus("Completed");
        }else {

            this.countDown--;
            this.countingDown = true;
        }



    }


    stopCountingUp(){
        this.countingUp = false;


    }
    stopCountingDown(){
        this.countingDown= false;

    }

    edit(name,priority,taskTime,status){
        this.name = name;
        this.priority = priority;
        this.taskTime = taskTime;
        this.status = status;
        this.countDown=taskTime;
        this.resetCountUp();
        this.resetCountDown();
    }
}
