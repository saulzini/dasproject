
class InfiniteTimer{


    constructor() {
        this.currentTime = 0;
    }

    resetCountUp(){
        this.currentTime=0;
    }


    resetAttributes(){
        this.resetCountUp();
    }

    incrementCountUp(){
            this.currentTime++;
    }

}
